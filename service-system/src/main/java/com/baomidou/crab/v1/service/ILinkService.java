package com.baomidou.crab.v1.service;

import com.baomidou.crab.v1.entity.Link;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 友情链接表 服务类
 * </p>
 *
 * @author jobob
 * @since 2019-02-07
 */
public interface ILinkService extends IService<Link> {

}
